"""
Pythonic way of Binary search algorithm

Author: Venkat K
"""
import math

def binary_search(obj, item):
    """

    :param obj:
    :param item:
    :return: True if the element is found else False
    """
    start = 0
    end = len(obj) - 1
    while True:
        mid_point = int(math.floor((start + end)/2))
        if item == obj[mid_point]:
            return item
        if item < obj[mid_point]:
            end = mid_point - 1
        else:
            start = mid_point + 1
        if start+1 == end:
            break

print binary_search([1,3,5,9,15,45,50,60,75], 45)

"""
Binary Search will performed on sorted array, divinding the array into two pieces and then comparing with the
elements based on mid value.

Worst-case performance	O(log n)
Best-case performance	O(1)
Average performance	    O(log n)
Worst-case space complexity O(1)
"""