"""
Pythonic way of Linear Search Algorithm.

Author : Venkat K
"""

def linear_search(obj, item, start=0):
    """
    This function will perform the linear search across the list and will
    return True if the element is found else False returned.

    :param obj:
    :param item:
    :param start:
    :return: True if the element found or False.
    """
    while True:
        for i in range(start, len(obj)):
            if obj[i] == item:
                return True
        return False

print linear_search([1,5,4,8], 10)

"""
Linear search will performed on entire array of element until the given element was found.

Performance Measure:

Worst-ase performance O(n)
Best-case performance O(1)
Average performance O(n)

Worst-case space complexity O(1)
Time Complexity:
User time (seconds): 0.01
System time (seconds): 0.00
Percent of CPU this job got: 92%
Elapsed (wall clock) time (h:mm:ss or m:ss): 0:00.01
"""